# Member Wellness

Member safety and health are important to FurHaven's staff team. However, please understand that FurHaven's staff team does not have formal training to provide one-on-one care. This article is a guide to help you or a friend reach out to individuals or organizations who are better equipped to provide assistance and care for mental well being.

If you are feeling depressed, suicidal, or facing other emotional health challenges, please read further on this page for some sources that can help you or a friend you are concerned for. If you find yourself concerned about another member of our community, please feel free to bring it to staff’s attention.

## Support
We encourage any member facing difficulties to reach out to someone that can provide support on a continuous/regular basis. People we recommend going to are:

* Family members (Parents, Siblings)
* Extended family (I.E. Uncle, Auntie, Grandparents, etc.)
* Friends
* Teachers or trusted staff member at educational institutions
* Religious figures or local community group members
* Counselors, psychologists, or therapists
* A doctor or local GP
* Local law enforcement/Police departments

**For emergency situations, where you or another person is at immediate risk, please contact local emergency services.**

## Extended Resources
If you do not feel comfortable with any of the above recommendations, we have compiled a list of support websites, hotlines, and groups.

### North America
**National Suicide Prevention Lifeline**

* +1 (800) 273-8255 (General)
* +1 (888) 628-9454 (Spanish)
* +1 (800) 799-4889 (Deaf/Hard of Hearing)
* [https://suicidepreventionlifeline.org/chat/](https://suicidepreventionlifeline.org/chat) (Online Live Chat)

### International
**Suicide Prevention Lifeline**

* [https://suicidepreventionlifeline.org/](https://suicidepreventionlifeline.org)
* [https://twitter.com/800273TALK/](https://twitter.com/800273TALK)

**Can’t find a number or website for your country?**
[https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines/](https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines)

We know this is a difficult topic and it can cause distress and worry for other people who may not be equipped to handle it. Therefore, please make sure you do not talk about depression, suicide, or other emotional health challenges within FurHaven. Instead, please seek help using the resources we have listed above and prioritize your health first.
