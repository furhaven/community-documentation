# Guidelines

FurHaven was created with the intention to be a fun, welcoming, and friendly environment for anyone to join. Our aim is for our community to be a positive and respectful space where cheating, inappropriate content, toxic behavior, and other negative topics are non-existent. To aid us in this goal, we have created a set of community rules for our members. We ask that all of our community members please honor them as a sign of respect not only towards our staff members but more importantly to your fellow members and our community as a whole.

These rules have been established to govern FurHaven. These rules are not to be interpreted how you like; There are no loopholes. Anyone claiming to not be breaking the rules due to it “not being in the rules” will receive an appropriate consequence. Our community staff members have been given leeway to interpret these rules and act in the best interest of the community as a whole; Staff members will evaluate all rules violations and punishment will be at their discretion.

Most of our rules are common sense items or advisories that should be easy for anyone to follow. Our community guidelines can be simply summarized using the phrase of **Don’t be an asshole; Don’t be an idiot**. – Simply put, it is treat fellow members as you would expect them to treat you in return.

We ask that any members, either present or future, please read and understand these rules thoroughly. By becoming a member of FurHaven’s community potential members declare that they have read and agreed to our community rules.

Ignorance of our community guidelines is not a valid excuse to avoid punishment and they are subject to change at any time without notification; It is the member’s responsibility to be knowledgeable about our community guidelines. If you are unsure whether something is against the rules, ask a staff member; Even if a rule is unclear, ask a staff member for clarification, as staff the interpretation of the rules is the only valid one.

If you opt to not agree to uphold our community rules, we ask for you to not become a member of FurHaven. If you are a current member of FurHaven and do not agree to all of our community rules and guidelines, we ask for you instead to cease access to and participation in FurHaven immediately as continued participation in the community constitutes an acceptance of the rules and guidelines as well as any changes hereafter.

## Member Respect
Do not abuse, belittle, or insult anyone; Treat those as you would expect them to treat yourself; Members are expected to treat each other with respect and dignity as well as show a degree of good humor, tolerance, and sportsmanship. If you think something may be disrespectful or inappropriate, simply refrain from saying it. Remember, you’re engaging with real human beings within our community.

1. Do not attempt to police or threaten others with punishment, commonly known as 'mini-modding' or 'backseat moderating'. Instead, we ask for you to report it to the staff team and let them handle it.
	1. To submit a report to the staff team for review, please use RedFox's .report command with the proper command syntax of `.report <User:Mention/ID> <Reason:Text>` in the #bot-spam channel; An example of the report command would be: `.report @RedFox User is posting unsolicited messages and links in chat`.
	2. If you are having a dispute with another member, or multiple members, or would like to submit a report with additional evidence, please follow through a ticket; As a reference for ticket creation, please refer to our dedicated support guide.
2. Discussion of controversial topics that typically lead to arguments (i.e. politics, religion) are to be kept out of the community. We understand that issues may arise with certain topics that may work their way into the conversation. We will allow light discussion of such topics if it relates to current events, however, if it devolves into an argument or veers too far off-topic, the topic of the conversation must be changed immediately upon the request of a staff member. We are a community based around games, not a community devoted to politics or religion.
	1. FurHaven believes in freedom of speech and welcomes all points of view as we believe it creates discourse within the community. FurHaven also, however, is not a community for political or religious discussion to take place in. FurHaven is a politically neutral community; We do not favor one side, viewpoint, or belief over another.
3. Do not discriminate based on race, ethnicity, nationality, region, gender, etc. – **All are welcome.**
4. Absolutely no witch hunting, harassment, or intention to harass is permitted. Do not incite violence against any person or entity; Doxing or harassing other members of the community is strictly off-limits and will result in immediate removal from the community.
	1. The posting of, or discussion about, another community member's personal information is strictly prohibited. This includes posting someone's real-life information such as their full name, physical address, etc.
5. Do not post any information that promotes self-harm, suicide, or threats against any community members; Do not promote or encourage other individuals of the community to commit acts of self-harm or threats against other members.
6. Community members may not impersonate someone in a misleading or deceptive manner.
7. Members of FurHaven are public representatives of our community in the larger furry/anthro community and thus are expected to consider the ramifications of their actions and to particularly avoid any course of action which would bring a negative public light or exposure to FurHaven or cause reputational damage.

## Illegal Content
Do not discuss or post explicit topics related to anything abusive, obscene, vulgar, slanderous, hateful, threatening, sexually orientated, drug-related, religious, political material including any other material that may violate any laws be they of your country or the country where FurHaven is hosted, The United States of America.

## Post Spam
FurHaven has a zero-tolerance policy regarding spam (also commonly called flooding); If a staff member deems something as spam, please stop. Spam can typically be defined as filling chat with unneeded/unrelated characters or messages; For our voice chats, spam is disrupting the voice chat channels with loud/obnoxious noises such as playing music through your microphone or using a soundboard.

1. The zero-tolerance policy regarding spam also applies to both microphone spam in our voice chats as well as advertisements for other communities.
2. Microphone spam is strictly prohibited in the voice chat of all Discord server channels and any games that FurHaven also partakes in that have a voice chat feature; If someone asks you to stop or just in general not do it (hence why this rule exists), please stop.
	1. Do not play music over your microphone or through your Discord connection to the voice channel on the server either through a virtual/emulated audio cable input or by holding your microphone close to a speaker.
	2. Push-To-Talk (PTT) or a low enough microphone trigger sensitivity to prevent background noise from being transmitted in the voice chats is recommended, but not required. Having your microphone constantly keyed/triggered or transmitting background noise, especially while not actively speaking, is a disruption to the voice chat. Push-To-Talk (PTT) is not a requirement for participating in the community voice chats but is more of a recommendation.
	3. Voice changers/modifiers and soundboards/sound effects are prohibited.
3. Some examples that would be deemed spam are, but are not limited to:
	1. Soliciting direct messages to community members who are not expecting it.
	2. Posting the same content/message(s) within a short time period.
	3. Exceedingly large images, ASCII art, and emoji spam.
	4. Unrelated/off-topic/link-farmed content.
	5. Shortened links (e.g. bit.ly), domain redirects, or disguising the source URL by any other means.
	6. Affiliate links (e.g. Amazon) and using pay-per-click websites/redirects (e.g. adf.ly).
	7. Harmful third-party content, such as malware, phishing, deceptive pop-ups, etc.
4. FurHaven is not an advertising platform; If you joined our community for the sole intent of advertising your services, third-party services, or any other communities, you will be removed.
	1. We allow our community members to share their services and communities that they are a part of if it is relevant to the current discussion, however, randomly posting an invite link to a Discord server, as an example, or advertising a service in any of the chats is spam.
	2. If you must advertise or would like to share another community with others, you must receive permission from a staff member. 
		1. The only exception to this rule is the dedicated #self-promotion text channel.
5. Please read the channel topics and pinned messages to ensure that you are staying on topic; Use discretion when uploading and sharing files to FurHaven as we ask that they get sorted into their appropriate locations.
	1. Files must be exclusively non-executable (i.e. `*.TXT`, `*.PNG`, `*.JPG`, `*.GIF`, etc.). If you need to send someone an executable file (e.g. a `*.EXE` program), send a direct link to the website that the software is hosted at rather than the file itself; This is simply to add more of a security/safety net as someone could tamper with the executable should it be directly uploaded through Discord.
	2. Files submitted in the formats of `*.GIF`, `*.MP4`, or `*.WEBM` must not be a static image. For `*.MP4` and `*.WEBM` files which feature a static image, audio must accompany the image to constitute 'non-static'.
	3. Artwork should be uploaded based on the content of the art itself. Female-based content would need to upload it to the appropriate channel under the 🍑 Fruit Stand category; The same would apply to male-based content which would need to be uploaded to the correct channel under the 🍭 Candy Store category.
		1. A more specific example would be if someone wanted to upload a piece of art from DiscordTheGE featuring a macro-female/giantess fennec fox sitting on a building; The artwork would need to be uploaded to the CHANNEL NAME HERE under the 🍑 Fruit Stand category.
	4. **WE HAVE A ZERO-TOLERANCE POLICY AGAINST ANYTHING THAT FEATURES THE SEXUALIZATION OF MINORS (LOLICON, SHOTACON) OR ANTHROPOMORPHIZED MINORS (BABYFUR, CUB) AND POSTING SUCH CONTENT OR ENGAGING IN DISCUSSION ABOUT IT WILL RESULT IN IMMEDIATE REMOVAL FROM THE COMMUNITY AND APPROPRIATE REPORTS BEING FILED.** If you are unsure whether or not content contains, encourages, or promotes pedophilia, child exploitation and/or abuse, or otherwise sexualizes minors, simply do not post it; It’s best to err on the side of caution and refrain from posting something that may be illegal and having to face the consequences.
		1. If you are engaging in role-play with other members of the community, age-play is strictly off-limits too; We don’t want to even entertain the thought of or idea here. If you must engage in age-play for whatever reason, it must be moved off of the Discord server to a direct message between all relevant parties; **Consider this to be your only warning.**
	5. Other NSFW content such as rape, gore, and scat is also prohibited.
6. FurHaven is an English speaking community. Communication, particularly viewable by other members of the community, will and must be conducted in the English language; Communication in other languages other than English are permitted in a private setting, of course.
	1. Leet Speak (e.g. “7YP1N’ L1NK 7h1z”) and alternating upper/lowercase letters, commonly known as AIT Case, (i.e. “tYpInG lIkE ThIs”) are difficult to read and will be marked as spam. Using Leet Speak and AIT Case in small amounts, such as in a sentence, quote, or in a joking sense is allowed.
7. There is to be no discussion of piracy, warez, leaked/stolen content, account trading/selling, etc.; Keep it legal!