# Member Ranks

## Initiate

Members of our community who have recently joined; Initiates are individuals who have permission to explore and participate in our community but will be limited to a certain extent as to what they can do. One example would be the Minecraft server, Aurelium, where initiates can join and play on the server network but will be restricted to where they can't make use of certain membership privileges, such as land claiming

## Member

Members are individuals who have applied for membership within the FurHaven community upon joining the community Discord server and have been accepted into the membership. Members have complete access to FurHaven's community and any of the services that FurHaven hosts or partakes in.

This rank is given to individuals who are accepted into FurHaven's membership.

## Founder

Founding members, Founders for short, are a select handful of individuals from FurHaven who were part of the community before it went public in 2020. The rank of Founding Member is purely cosmetic in nature and does not grant any special permissions to members who have this rank.

**This rank can no longer be acquired as of January 1st, 2022.**

## Guide

Guides are members of the FurHaven community who have shown through merit to always assist other members of the community who are in need as well as answer any questions that members of the community may have. They have strong communication and teamwork skills that show when they are helping others in the community. Guides are also highly knowledgeable of and have a passion for FurHaven and the services offered.

One example of a guide could be an individual who has run multiple raid sherpas in Destiny 2 as well as introduce members to the game and educate them on the various gameplay mechanics.

This rank is granted to members of the community who have shown through merit to be the ones who always go above and beyond, they put others before themselves. This rank is earned over an extended period of helping others in the community and is rarely granted to members.

## Moderator

Moderators are individuals of the FurHaven community who help mediate discussion within the community, settle disputes, and provide assistance to any members who are in need. Moderators are part of the communal staff team. The rank of moderator is granted to members of the community who have shown to be mature individuals that can handle intense situations and are knowledgeable about the different aspects of FurHaven.
FurHaven will rarely ever ask for staff applications; We will approach an individual if we believe they would fit the position. Please DO NOT ask to become a staff member; We will announce when we need additional staff

## Administrator

Administrators are individuals of the FurHaven community who run the community and manages its services. Administrators are the highest rank of the community and are part of the staff team. The rank of administrator is not assigned or given to members nor are applicants being accepted.
