# Chat System

On Aurelium, in-game chat is available for players to partake in; It is also divided into multiple channels with specific topics to keep chat organized. By default all channels are opted into upon your first time connecting to the network; After this, you may opt to leave specific channels with a few exceptions to some channels such as the Network Alerts channel.

## Channel Directory
Players on Aurelium have the option of registering a unique ID to their tools, weapons, and armor. Having a unique ID assigned to your item allows it to possibly be recovered should your item ever become lost or stolen. To manage item ownership, the following commands have been created:

### Chat Channels
* **Global** (`/ch global` or `/ch g`)<br />
This is the main network-wide channel. You can chat here with whoever is currently part of this channel. If it gets too spammy and noisy just leave it and stick to the local channel to chat with people currently around you.
* **Local** (`/ch local` or `/ch l`)<br />
This is the local channel that consists of chat specific to the server that you are currently connected to. Here you can only hear people/speak to people that are within 1000 blocks away from you in any direction. This channel is especially useful when you are working on a project with someone; If you want to focus with the people in your area only, just leave the global channel. This way only people that are near you will be heard. If you need to speak to someone either join the global channel back or if no one is there, message someone that is online with the `/msg` command.
* **McMMO** (`/ch mcmmo` or `/ch m`)<br />
This is the McMMO party channel. This is our alternative solution to McMMO's party chat system as it allows all players of a party to have their own private party chat to communicate with each other, even if they are spread across the network on separate servers. All players have access to the McMMO party channel however they must be in an McMMO party in order to send and receive chat messages into the channel; If you join the McMMO party channel without being in a party, your messages that you send will not be viewable by anyone as only players in the same party can view party chat messages.
* **Staff** (`/ch staff` or `/ch s`)<br />
This is the staff only chat channel. Only staff members of FurHaven have access to this network-wide channel.

### Info Channels
* **Server Alerts**<br />
This channel is where all server related alerts and announcements will be posted. You cannot send messages in this channel, nor can you leave it. It is simply a channel with the sole purpose of allowing all server alerts to get to you.

## Chat System Commands
The benefit of using protection claims on Aurelium is that all inventories that are placed within a protection claim are automatically locked. One downside to this, however, is that the inventories are locked to the claim, not the player; This means that not only the owner but also anyone else who has been added to the claim through the `/trust` command can also open the inventories within the claim.

Another downside of using protection claims is that should a protection claim disappear due to inactivity or disbandment, all inventories that were in that claim are now unlocked for anyone to access. In short, protection claims work and they will protect any inventories that are placed within them it’s just advised that players should be vigilant with who they trust with their claims as well as make sure that all inventories placed in the world are actively protected by a claim.

To create a claim and reference a more in-depth guide, players should refer our [GriefPrevention guide](../griefprevention-claims).

### Channel

### Player
* **/ignore**<br />
Stop viewing a player's chat messages, toggle ignore for other players or list all ignored players. Ignoring a player will also block them from private/direct messaging you.
	* **/ignore list**<br />
	Display a list of players that you are currently ignoring.
	* **/ignore <player>**<br />
	Toggle whether the provided player is ignored or not.
