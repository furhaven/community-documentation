# McMMO Parties

On Aurelium, players can create their own parties that you can invite friends and fellow players to. Parties provide players the ability to use a separate party chat that is outside of the mainstream chat channel, form alliances with other player parties, share item drops, and even share earned EXP.

## Creating A Party
Parties can be created on Aurelium by players using the **/party create <name\>** command. Upon creating a party, players can use the **/party** command to view current statistical information about their party as well as information about players who have joined their party.
