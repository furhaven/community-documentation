# Discord Linking
One of the requirements that all members must do in order to properly access the Minecraft server's gameplay features is to link their Minecraft UUID with their Discord account ID. Upon your first time joining Aurelium you will be assigned the role of Guest and will need to link your accounts in order to gain access to the rest of the server. 

By having your accounts linked, it allows for roles and permissions to sync as well as analytics to be collected about your time and actions on the server such as login times, player activity, and time spent on the server. It also provides an additional layer of security by only allowing members of FurHaven who are in the Discord server access to the server and its gameplay features.

## Linking Accounts
As previously mentioned, account linkage is a requirement on Aurelium. If a member utilizes alternate accounts, only their main Minecraft account UUID is required to be linked to their Discord account.

1. Connect to Aurelium using your Minecraft client. Once you have successfully connected to the server, run the `/discord link` command; A message containing a randomly generated link code will be presented to you in chat.
2. After you have received your link code, focus on the Discord server in your Discord client and send a direct message to the Aurelium Discord bot. The bot will respond back with a message declaring that your Discord account has been linked to your Minecraft account.
3. Upon a successful link of your Discord account to your Minecraft account, your permission group on the Minecraft server will be updated to reflect the member role that you have in the Discord server. You now have access to all gameplay features of the server.

If you have encountered any errors in the process of linking your Discord account to your Minecraft account, please refer to the [Common Errors](#common-errors) section of this article. You may also opt to reach out to a member of staff for support should your error remain unresolved.

## Unlinking Accounts
You may not unlink your Minecraft UUID from your Discord account; You will need to reach out to a staff member and request for your accounts to be unlinked if you wish to have them separated.

This system is in place because it allows us to keep track of which Minecraft UUID is associated with each user of our community. If you have multiple accounts, it is advised to notify staff about your use of alts and to only link your primary account's UUID to your Discord account.

## Common Errors
While undergoing the process to link your accounts, the following errors may be encountered:

* **Invalid link code! - Unknown code.**</br>
The link code that you sent to the bot is not recognized by the bot or has expired. To get a new link code, simply use the `/discord link` command in-game. 
* **Invalid link code! - Invalid format.**<br />
The link code that you sent to the bot is in an invalid format. Link codes are four digits long and should only be sent as is to the bot in a message without any extra characters or words. For example, if someone's link code is `1234`, they would only need to message `1234` to the bot without any extras.
* **Your Discord account is already linked!**<br />
Your Discord account was detected as already being paired with an existing Minecraft UUID. Should the Minecraft UUID linked to your account not match the UUID of your current Minecraft account, it is recommended to reach out to a staff member to unlink the accounts.
