<p align="center">
  <img src="https://docs.thengx.network/assets/images/minecraft/minecraft_logo.png" />
  <b>Start your adventure! FurHaven.XYZ</b>
</p>

# Aurelium Realms
Aurelium Realms, "Aurelium", is a private whitelisted Minecraft server only accessible by members of the FurHaven community. Individuals who wish to join the server must be verified, active members of the FurHaven community in order to be added to the whitelist. Anyone who wishes to join the server that is not a member of FurHaven, must join the community before being granted server access.
