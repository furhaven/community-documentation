# Troublesome Players

Sometimes, other players on Aurelium may annoy you or you may not want to be bothered by someone while playing. If you come across a player’s messages that you don’t want to see, we encourage you to take steps for self-moderation. To stop seeing another player’s messages in chat on Aurelium, use the:

## Using the Ignore System
FurHaven offers the solution of ignoring players on Aurelium through the use of a player ignore system. Players are recommended to use this command over Minecraft’s built-in Social Interactions Screen as the list of ignored players is remembered and doesn’t reset between disconnects from the server. To interact with the player ignore system and manage ignored players the following commands are available:

* **/ignore <username>** <br />
Ignore the target player’s chat messages
* **/unignore <username>** <br />
Un-ignore the target player’s chat messages
* **/ignore** <br />
Lists all players that you have chosen to ignore

## Social Interactions Screen
The Social Interactions Screen is a built-in feature of the vanilla Minecraft client and it allows players to hide messages of other players. To open the Social Interactions Screen, use the default keybind of `P` and you will be met with a list of players currently logged on the server; From here, you can hide messages from selected players on the server by clicking the chat balloon button to toggle if chat messages are hidden. If you wish to hide a player’s chat messages across servers, you will need to block them via the Social Interactions Screen. Messages of players that you have blocked will never show up in your chat, even across servers; They will also be blocked from sending you any invites to Minecraft Realms.

Please note that any players whose messages have been hidden will be reset when you disconnect from the server; You will need to re-hide the player’s messages from view again via the Social Interactions Screen or block them completely via your Microsoft account.

## Reporting Harassment

If you are being threatened or harassed by another member of the community, please bring it to the staff team’s attention. Another recommended method is to create a support ticket as you will have a place to discuss the matter with multiple staff members as well as resolve the issue with the member, if needed; To create a ticket, please refer to our ticket creation guide.

As a gentle reminder, if you have any questions or concerns with anything or anyone in the community, please contact the staff team; We **cannot** help anyone if we are not aware of an issue.