# Performance Guide

Memory usage of Minecraft has changed over time with each new release either adding to or subtracting from the minimum amount of memory required to run the game at optimal performance. The general trend is that the memory footprint of Minecraft has increased over time and is majorly due to the changing vanilla landscape.


## Initial Steps
Before we can optimize Java and Minecraft's in-game settings we should first take some initial steps into identifying what the root cause(s) of our performance drops are. The two main drivers behind performance issues in Minecraft, besides hardware, are applications that are consuming large amounts of resources and outdated/buggy Java JVM installations.

### Closing & Disabling Apps
CPU usage is one of the main factors that influences performance in Minecraft. If your processor is constantly under load and is hitting peaks of over 80% usage while playing Minecraft, it is recommended to attempt to close some background applications that may be using your CPU runtime cycles alongside Minecraft.

Some examples of applications that will typically use large amounts of CPU cycles are Adobe applications (Photoshop, Illustrator, Evolve), recording suites, media players, and web browsers; Not all applications use large amounts of CPU cycles but it is recommended to watch for those that do and to not make use of them while playing Minecraft. 

Applications that launch alongside your operating system, startup applications, are also a large factor in consuming CPU cycles; You can disable startup applications on Windows using this [article](https://www.howtogeek.com/74523/how-to-disable-startup-programs-in-windows/) as reference.

### Updating Java
With the release of Minecraft's Caves & Cliffs update in 2021, Minecraft 1.18 has raised the minimum required Java version to Java 17 LTS for clients and servers alike. The major distributors of Java are [Oracle](https://jdk.java.net/), [Azul](https://www.azul.com/downloads/), [Adoptium](https://adoptium.net/), [Amazon](https://docs.aws.amazon.com/corretto/), and [Eclipse](https://projects.eclipse.org/projects/technology.openj9) with Azul's Zulu builds of Java being the recommended build for use with Minecraft.

To install and/or update Java, please refer to this [tutorial article](https://minecraft.fandom.com/wiki/Tutorials/Update_Java) on the Minecraft wiki.

## Optimizing Java & Minecraft

### Setting Java Arguments
One of the first steps that we can take towards improving our client's performance is to tweak Java's JVM that Minecraft is built upon and runs on. The following Java arguments have been the go to standard for the modding community of Minecraft and will also improve performance for vanilla clients.

```java
-Xmx4096M -Xms4096M -XX:+UseG1GC -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=51 -XX:G1HeapRegionSize=32M
```

An explanation of each of the Java arguments are as follows:

* **-Xmx4096M -Xms4096M** <br />
This flag configures the heap size to four gigabytes and keeps it pinned at four gigabytes. Minecraft tends to use a large amount of memory. It is recommended that the `-Xmx` and `-Xms` flags be the same value as it will guarantee that the heap given to the JVM for Minecraft will be fully utilized by Minecraft; If the heap needs to inflate, due to a lower heap minimum size, to the maximum size it may result in out of memory errors due to conflicts with other applications that are making use of the system memory. <br /><br />To change the amount of memory allocated to the JVM for Minecraft, multiply 1024 by the number of gigabytes that you wish to allocate; For example, three gigabytes is would be `1024 * 3` for a heap size of `3072`; The flags would be `-Xmx3072M -Xms3072M` to allocate three gigabytes to the JVM. It is also advised to not set the heap any amount lower than two gigabytes (`-Xmx2048M -Xms2048M`).<br />

	!!! warning "JVM Heap Allocation Limits"

		A general rule of thumb for setting Minecraft's JVM heap size is to keep it at or above at least three gigabytes and to not exceed allocating more than six gigabytes, or more than 50% of your total system memory, to the JVM with a max limit of whichever comes first.

* **-XX:+UseG1GC** <br />
This flag enables the G1GC garbage collector; This is a great garbage collector for interactive applications, such as Minecraft. It tries to keep garbage collection predictable, so it never takes a long time (big lag spikes) and doesn't repeatedly take lots of short times which may lead to micro-stuttering.
* **-Dsun.rmi.dgc.server.gcInterval=2147483646** <br />
This flag configures the RMI (Remote Method Invocation) layer not to run a full garbage collection cycle every minute.
* **-XX:+UnlockExperimentalVMOptions** <br />
This flag unlocks and enables the usage of experimental JVM arguments.
* **-XX:G1NewSizePercent=20** <br />
This flag configures G1GC to put aside 20% of the heap as "new" space. This is space where new objects will be allocated, in general. You want a decent amount, due to Minecraft making a large amount of objects (such as BlockPos) and you don't want to have to run a collection whenever it gets full as this is a big source of micro-stutters.
* **-XX:MaxGCPauseMillis=50** <br />
This flag configures the G1GC to try and not stop for more than 50 milliseconds when garbage collecting, if possible. This is a target, and G1GC will ignore you if you put a silly number in like 1 which is unattainable. 50 milliseconds is the equivalent time of one server tick.
* **-XX:G1HeapRegionSize=32M** <br />
This flag configures G1GC to allocate it's garbage collection blocks in units of 32megs. The reason for this is that chunk data is typically just over eight megabytes in size, and if you leave it default of sixteen megabytes, it'll treat all the chunk data as "humungous" and so it'll have to be garbage collected specially as a result. This will especially help in modded causes as some mods tend to cause humongous allocations as well, such as JourneyMap.

### Changing Client Video Settings

### OptiFine (Vanilla & Forge)
OptiFine is an optimization mod for Minecraft created by sp614x. It allows Minecraft to run at a higher, smoother frame rate by optimizing various aspects of the game, such as world rendering; It also includes additional graphical features as well such as shader support.

#### Installing OptiFine

#### Configuring OptiFine

### Sodium (Fabric)
Sodium is a rendering engine replacement for the Minecraft client which greatly improves frame rates and stuttering while fixing many graphical issues. It presents the player with a similar set of features that OptiFine offers, but for the Fabric modding API instead.

As detailed by [JellySquid](https://modrinth.com/user/jellysquid3), [by design, Sodium only optimizes the client rendering code](https://modrinth.com/mod/sodium#:~:text=Note%3A%20By,entire%20collection.). You should also install their other mods of Lithium (game physics/AI) and Phosphor (lighting) to optimize the other parts of your game. This is done so that players can pick and choose which mods they want to use to optimize their game, but we generally recommend using all three alongside a fourth mod, FerriteCore (memory optimization), as it offers the best performance returns.

#### Installing Sodium
To begin optimizing your Minecraft installation with Fabric, we recommend using all four of mods of Sodium, Lithium, Phosphor, and FerriteCore for the best results; The installation of these mods has been covered in the following steps below:

1. Download and install the version of Minecraft that you wish to optimize. We recommend creating a separate profile on your launcher for this to prevent Fabric and any mods from overwriting your Vanilla installation profile.
2. Ensure that you have the proper version of [Fabric](https://fabricmc.net/use/installer/) and [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api) installed for your Minecraft version; If not, you can find the downloads and installation guides on [Fabric's website](https://fabricmc.net/).
3. After verifying that you have the correct version of Fabric and Fabric API installed, download the [Sodium](https://modrinth.com/mod/sodium), [Lithium](https://modrinth.com/mod/lithium), [Phosphor](https://modrinth.com/mod/phosphor), and [FerriteCore](https://modrinth.com/mod/ferrite-core) mods from their respective Modrinth pages, and place the jars in your mods folder within your Fabric installation.

You should now be able to launch your Fabric's installation profile from your launcher. Once you are in-game, you should be able to go into your `Video Settings...` and begin setting the provided options to what fits your needs best.

#### Configuring Sodium
When it comes to configuring Sodium, there is no "one size fits all" solution as various factors, especially hardware such as CPU/processor and GPU/video card, play a major role in the end result of your optimization efforts. With the four mods of Sodium, Lithium, Phosphor, and FerriteCore installed, the default settings upon launching your Fabric installation should provide a considerable improvement compared to when you were running the game without these mods.

As mentioned, the default settings should provide a generous performance improvement; Should you want to tweak some settings, we recommend experimenting with the following settings in your `Video Settings...`:

* Setting `Chunk Update Threads` to one or two threads and enabling `Always Defer Chunk Updates` under the `Performance` tab.
* Testing various frame values for the `CPU Render-Ahead Limit` under the `Advanced` tab.