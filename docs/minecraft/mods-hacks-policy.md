# Client Mods & Hacks Policy

Minecraft modifications are a large part of the Minecraft experience for many players, with modifications ranging from small performance and visual improvements right through to gameplay altering ones. As Minecraft modifications provide such a wide range of functionality, however, not all modifications are permitted to be used on our server. This guide outlines what types of modifications we do allow on the server.

## Preface

Before using any modifications please be aware that all use of modifications on our server is at your own risk, including the ones we list as examples in this guide. We cannot review every version of every modification, and as such we cannot provide any guarantees of modifications being within our server rules.

Any modifications you do install should always be obtained from the official source for that given modification, be that the creator’s website, GitHub, CurseForge, or other profile (e.g. YouTube channel). Unofficial downloads from other sources may not behave as expected, which could include bugs or unadvertised features (which could result in punishments on our server), or can even be malicious in nature (e.g. viruses, spyware, etc.); A list of unsafe mod download websites can be found on the [StopModReposts project website](https://stopmodreposts.org/sites.html).

## What Is and Isn't Allowed
Provided below is a list of the categories of modifications we permit on the server, along with an explanation of what each of these categories does and does not include and permit. If a modification does not fit into one of these categories, it should be assumed that it is disallowed by default.

* **Performance Modifications** <br />
Modifications that simply seek to improve the performance of the Minecraft client without making changes to the game itself, such as those which improve the FPS of the Minecraft client.
* **Aesthetic Modifications** <br />
Modifications that change only the look and feel of the game without modifying gameplay, such as standard shader modifications or even resource packs. These must not, however, change the properties of blocks (e.g. make non-transparent blocks transparent) or change the player’s perspective (e.g. allowing them to see around or over objects they normally wouldn’t be able to). Modifications that also alter the brightness and gamma settings of the Minecraft client fall into this category.
* **Quality of Life Modifications** <br />
Modifications which alter the look and feel of the in-game head-up display (HUD), without adding extra information which would normally be unavailable to the player. For example, HUDs adding armor and status effects, which are available to the player in their inventory screen, are permitted, while mini-maps, other player health/armor indicators, player distance/range, etc. are not. Modifications may also add additional features to the client that aid in inventory management, such as automatic organization.

**If a modification does not fit clearly into any of the allowed modification categories, it should be assumed that it is disallowed on the server.**

## Allowed Client Modifications
These are a small handful of modifications that you can use on Aurelium. For the most part, these are cosmetic modifications or modifications which show limited amounts of additional information which should not give you an in-game advantage.

* **OptiFine/Sodium (Client Optimization Mod)** <br />
Client modifications which have the main goal of improving client FPS (Frames Per Second) by optimizing the way Minecraft operates and offering more customizable Video Settings.
* **Shaders Mod, Sildurs Shaders, etc. (Shaders)** <br />
Client modifications that change the visual appearance of the game to make it more aesthetically pleasing.
* **Inventory Tweaks, Inventory Sorter, etc. (Inventory Management)** <br />
Client modifications that allow for inventory management to be automated or made easier.
* **Replay Mod (Gameplay Recorder)†** <br />
Client modification which allows for the recording and playback of gameplay, while also allowing for the changing of perspective during playback (but not in live gameplay).
* **Badlion Client, etc. (Clients/Mod Packs)‡** <br />
Minecraft client mods (sometimes made available as self-contained clients) which contain a variety of modifications grouped together into a single package are permitted, provided the included modifications are permitted on our server by themselves.
* **F3+B (Minecraft Debugging)** <br />
Built-in debug tool in the vanilla Minecraft client which shows entity (player, mob, etc) hitboxes. It is an official feature of Minecraft; Not a client modification

!!! info "Player Reports & Mod/Client Usage"

	† Unfortunately we are not able to accept evidence for reports submitted to us which have been recorded using these modifications. This is due to the potential for these reports to be edited or falsified, leading to false reports.

	‡ We recommend that anyone opting to use custom clients that are not using vanilla jars to do their own research into the background of those clients before use. FurHaven recommends using the vanilla Minecraft jars downloaded from Mojang. Please also note that this does not include cheat/hacked clients or similar, any of which is against our rules (see below).

## Disallowed Client Modifications
Although we do not have a list of disallowed modifications, a general rule of thumb is that any modification which provides the player with any significant advantage over others on the server is disallowed as it goes against our rules.

Additionally, we would also like to note that anything which automates any player gameplay action is strictly disallowed, be those Minecraft modifications, external software, or hardware. Some examples that are included in this are auto/burst clicking buttons or macros, auto-sprint, aim assists, and bots/self-bots (such as Baritone).

Finally, modifications that alter the way in which your Minecraft client interacts with and communicates with the server are also disallowed. Please ensure that any modifications which are used are strictly client-side only, with them not changing or altering the behavior of the game.

## Hacked Clients
Hacked clients (also commonly called cheat clients, utility mod, or even just 'client') that include modifications that provide players with unfair advantages over others on Aurelium are strictly prohibited. Players caught using any client deemed to be a hacked client, may be automatically banned by Hyperion; Players may use hacked clients outside of Aurelium, but while accessing the server, it is recommended to use a vanilla client instead to avoid a potential ban.

## Disclaimer
FurHaven does not endorse any of the mods named above, which are provided solely as examples of commonly used modifications on our server. FurHaven nor any of its staff members are the developers of any of the named modifications unless explicitly noted, and we do not have control over those modifications. We are not responsible for the behavior or actions of any modification, which may be updated or changed (even automatically) at any time by their individual developers; Modifications are installed on a player’s client at their own risk. If your mods don’t follow the guidelines linked above, we can’t guarantee that you will not be banned by our cheat detection systems.
