# Common Scams

If you received a direct message from an account claiming to be Discord Staff, HypeSquad, Partners, Moderators, etc. that they would like you to submit an application, claim a Nitro gift/boosts, join their community, or needs to reach out to you regarding a support ticket, it is a phishing attempt to try and gain access to your account.

The same applies for any other messages that are along the lines of testing out a game or program for free early access or compensation; Some scams are simple "verification" scams where they require you to log in to Discord in your web browser and then drag and drag your Discord tab over their verification website or some link, code, script, etc. onto your Discord tab.

!!! warning "Account Security"
	
	Do not sign into any links send to you in a message, they are not legitimate and your Discord account, or client token, will be stolen. If you are required to log in to your Discord account, verify that it is done through the OAuth2 login gateway (URL of `https://discord.com/oauth2/`); Only **discord.com** and **support.discord.com** domains should be trusted as official Discord domains.

	If you have entered your information into one of these sitesyou should immediately:<br />
	**1.** Change your password immediately (User settings > My account)<br />
	**2.** Enable two-factor authentication: <https://dis.gd/2fa><br />
	**3.** Review safe account practices: <https://dis.gd/common-scams><br />
	**4.** Review how to protect yourself from future scams.

## Official Correspondence

Discord will never send official communications to anyone via direct message from a user account, bot, or website. If you have been contacted by such means, please report it to Discord using the red `Report Spam` button at the top of the direct message history.

Official messages from Discord are only ever sent to users using a bot labeled with a `SYSTEM` indicator on the client, or from a **discord.com** or **support.discord.com** email address. Do not communicate with anyone claiming to be a Discord support agent who is communicating with you outside of **support.discord.com** or **discord.com** email domains; Engaging with such parties puts your account at risk for potential theft.

!!! info "Account Tokens"

	Discord support will never ask you for your account's login token, and you should never have any reason to open your browser's developer console in the first place. Please note that this is only applicable to Discord on your internet browser, and not the desktop or mobile application.

In addition to the information above, the only official Discord support outlet is hosted at <https://support.discord.com/>, utilizing Zendesk as a backend. Support occurs via that website and email; The only email domain that Discord uses for support purposes is **@discord.com**.

If you have received communications from, or have been asked to visit, any support domain that is not <https://support.discord.com/>, such as other Zendesk domains or live chat services hosted on other platforms like crisp.help, that is not Discord support; Do not engage with it and report it.

For more information, please refer to <https://dis.gd/system-messages>.

!!! warning "Official Discord Communications"
	
	Any communications that you receive from someone alleging to be Discord staff that do not originate from https://support.discord.com/ or from a bot labeled `SYSTEM` in a direct message should be considered illegitimate.
	
	Do not communicate with anyone claiming to be a Discord support agent who is attempted to reach out to you outside of support.discord.com or discord.com email domains. Doing so leaves you at risk for account theft.

## Examples of Scams
### Fake Games & Programs
With this scam, you will receive a direct message from a user asking you to download their game, software, code, etc. and to check out and run for them as a small playtest or with some form of compensation being offered. Regardless of how the backstory from the individual plays out, they will typically always ask you to download a file or navigate to a URL that they provide that allows them to enter your computer and/or compromise your Discord account.

The alternative to this scam is where they ask you to run some code in your browser using its built in developer console; You will typically be asked to log in to Discord on your browser and to then run the code that they have sent you in your browser's console while focused on your Discord tab. Running this code will grant them access to your Discord's client token allowing them direct access to your account, bypassing any login checks and 2FA that you may have had.

### Giveaways & NFT Drops
### Discord Impersonation
Impersonation scams involve a malicious party that reaches out to you acting as if they are official Discord 

### Free Nitro/Server Boosts
One of the most common scams that you may encounter on Discord are free Discord Nitro and server boost scams. These scams typically require you to login, scan a QR code with your Discord app, or invite a bot to your Discord server.

Official Discord gifts will use the `discord.gift` domain and will generate a special embed on the Discord client, as shown in [this image (in-app)](https://support.discord.com/hc/article_attachments/360100559094/Screen_Shot_2021-01-19_at_5.32.44_PM.png) or [this image (with a direct link)](https://support.discord.com/hc/article_attachments/1500001750501/Screen_Shot_2021-01-19_at_5.38.00_PM.png). Scams are not possible through the official client.

## Compromised Account
If your account has been sending these kinds of messages, your account has been compromised. If you...

* **...downloaded and executed malware**<br />
You should try and use a different device entirely to change your password (e.g., your phone). You should then follow [these steps](https://support.discord.com/hc/en-us/articles/115004307527--Windows-Corrupt-Installation) to fully uninstall Discord, run a complete anti-virus scan, and then re-install Discord. If your account is compromised again when logging in afterward, you may need to factory reset your computer and reinstall your operating system.
* **...entered your password into a malicious/fake website**<br />
You should change your password, setup SMS verification, and enable 2FA on your account.
* **...did something else that isn't listed**<br />
You should change your password, setup SMS verification, and enable 2FA on your account.

### Securing Your Account
After you have managed to regain access to your account, you should conduct the following to not only lock the attacker out of your account but to also secure it further with an additional layer of security:

* **Reset your Discord account's password**<br />
Changing your password will allow you to log the individual out of your account and prevent them from further accessing your account as your old password will no longer be able valid.
* **Enable Two-Factor Authentication & SMS Backup**<br />
Enabling Two-Factor Authentication will provide you with an extra layer of security when it comes to logging in to your Discord account. With 2FA enabled, when you go to log in to your Discord account, Discord will now require you to enter a pin upon login that is randomly generated in your authenticator app. This allows you to verify that you are the individual who is accessing your account since if someone gets ahold of your password, they will be unable to login since they will not have access to your authenticator. Examples of recommended authenticator apps are [Google Authenticator](https://support.google.com/accounts/answer/1066447) ([Android](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) / [iOS](https://apps.apple.com/us/app/google-authenticator/id388497605)) and [Authy](https://authy.com/).

## Reporting Malicious Content
If you believe that you have received a possible scam from a bot or user account, consider the following before interacting with the account:

* Is relatively new, unfamiliar, or unverified (if it's a bot) contacting you unprompted.
* Are not from Discord in the form of an email or [System-tagged account](https://support.discord.com/hc/en-us/articles/360036118732)
* Has poor grammar, spelling, or misuses punctuation/capitalization
* Offers items that feel "too good to be true"

If the account continues to send you messages, you can:

* Use Mutual Servers on the account's profile page to determine what servers they share with you to alert the staff of that server as well as disable Direct Messages from Server Members for that server.
* Disable direct messages from all servers in the `Privacy & Safety` section under your User Settings.
* Block and report the account to Discord using the integrated `Report Spam` button in the direct message, [as shown here](https://assets-global.website-files.com/5f9072399b2640f14d6a2bf4/61fb18b9858fe1dcb545ec01_5nSurukZcGFFFZd2fy7rNC93Da3CPtpvG0cNEJfEKzKyvHPWxqlGWpjXA1I6PcNelWtTLho4cU6i7T2WAfiCMJ05YgKY5f5-OgowIeoPt2u3hXoWYndHzxRGayEIcBydlNSKK1lN.png)
* [Submit a ticket](https://dis.gd/report) to Discord's Trust & Safety team; You can learn about how to properly report content through [this support article, titled: How to Properly Report Issues to Trust & Safety ](https://support.discord.com/hc/en-us/articles/360000291932)

## Additional Resources
* [Scams and What to Look Out For](https://discord.com/blog/common-scams-what-to-look-out-for)
* [Protecting Against Scams on Discord](https://discord.com/blog/protecting-users-from-scams-on-discord)
* [Claiming a Nitro Gift FAQ](https://support.discord.com/hc/en-us/articles/1500001829622-Claiming-a-Nitro-Gift-FAQ)

## Account Support

FurHaven is not the most appropriate location to ask for nor receive support for account-related issues pertaining to your Discord account. These typically include issues related to login, payment, and verification.

FurHaven is operated by a group of individuals who do not work for Discord and the community is incapable of solving these issues; The best way to get support for such issues is to:

* Search [the Discord support knowledge base](https://support.discord.com/hc/en-us)
* [Submit a ticket](https://dis.gd/contact) to Discord's support team

If you have submitted a ticket, please be patient and wait for a reply. The support team will get back to you as soon as possible.

## Closing
The above tactics are some of the ways that scammers may attempt to socially-engineer you into giving up your information. Even if you don’t click any of their links, it's best to simply block and report them to us, rather than engage further.

We also have provided some additional resources that we recommend viewing to allow yourself to get familiar with how to handle these accounts while on Discord. For questions regarding your account or messages you may have received from suspicious individuals, we urge you to report it to our staff team as well as reach out to Discord Support directly at <https://dis.gd/contact>.

Other than this notice and any other important updates regarding scams and account security, FurHaven, nor its staff, are not allowed to provide you with support for your Discord account; Furthermore, the staff of FurHaven will never ask you for your password/login credentials.