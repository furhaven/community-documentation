# Commands

If you received a direct message from an account saying that Discord Staff, HypeSquad, Partners, Moderators, etc. would like you to submit an application, join their community, or needs to reach out to you regarding a support ticket, it is fake; The same applies for any other messages that are along the lines of testing out a game or program for free early access or compensation.

Some scams have also required you to "verify" in a browser by logging into Discord in your browser and then dragging and dropping your Discord tab over their verification website.

## Bot Commands
### Fake Games & Programs
With this scam, a user will send you a direct message asking you to download their game, software, code, etc. to check out and run for them as a small playtest or with some form of compensation being offered. Regardless of how the backstory from the individual plays out, they will typically always ask you to download a program or navigate to a URL that they provide that allows them to enter your computer and/or compromise your Discord account.

The alternative to this scam is where they ask you to run some code in your browser using its built in developer tools; You will typically be asked to log in to Discord on your browser and to then run the code that they have sent you in your browser's console while focused on your Discord tab. Running this code will grant them access to your Discord's client token allowing them direct access to your account, bypassing any login checks and 2FA that you may have had.

!!! warning

	Discord will never ask you for your token, and you should never have any reason to open your browser's developer console in the first place. Please note that this is only applicable to Discord on your internet browser, and not the desktop or mobile application.

### Giveaways & NFT Drops
### Discord Impersonation
Impersonation scams involve a malicious party that reaches out to you acting as if they are official Discord 

### Free Nitro/Boosts
One of the most common scams that you may encounter on Discord are free Discord Nitro and server boost scams. These scams typically require you to login, scan a QR code with your Discord app, or invite a bot to your Discord server.

## Closing
The above tactics are some of the ways that scammers may attempt to socially-engineer you into giving up your information. Even if you don’t click any of their links, it's best to simply block and report them to us, rather than engage further.