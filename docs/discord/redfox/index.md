# RedFox Discord Bot

RedFox is a customized fork of the popular Discord bot, [RedBot](https://github.com/Cog-Creators/Red-DiscordBot). She was designed to serve the FurHaven community as well as watch over it and keep order.

RedFox is a general purpose Discord bot that offers various commands that members of the FurHaven can make use of.