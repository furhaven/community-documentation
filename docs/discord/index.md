<center>
	![discord_logo](../assets/images/discord/discord_logo.png)
	[Join our community Discord server for announcements and discussion!](https://discord.xeriscape.network/)
</center>

# Discord

To make it as easy as possible to write documentation in plain Markdown, most UI components are styled using default Markdown elements with few additional CSS classes needed.
{: .fs-6 .fw-300 }
