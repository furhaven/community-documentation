# Channel Directory
This is a breakdown of our community Discord server's various channels that are available to our members. The channels listed here have been given a description of that channel's purpose as well as a direct link to that channel for viewing; The channel link will not work unless you are a verified member of the community however.

## Lobby
The Lobby channel category contains a list of channels that non-verified members of the community Discord can view. This category exists to educate individuals about the means of becoming a proper member of Furhaven.

### Gateway
The `#Gateway` channel is the first channel that individuals are presented with upon joining FurHaven's Discord server. This channel provides individuals with background information about the community as well as details about how to become a proper community member.

### Guidelines
The `#Guidelines` channel is the dedicated channel for FurHaven's community guidelines. A summary of the community guidelines has been provided here for viewing with a link to the much larger, detailed guidelines for future reference.

## Information
Your one stop shop for all things related to our community; Community announcements, events, and general chat is the main focus here!

### Announcements
`#Announcements` is a member's one stop shop for all information regarding community updates, announcements, and events. It is a forum channel that has been converted into an announcement channel with any announcements getting their own dedicated thread.

### Bulletin
The `#Bulletin` is a forum channel that serves as a resource area for members of the FurHaven community. Individual topics have been granted their own forum post that can be used as a reference in conjunction with the community [support documentation](https://docs.furhaven.xyz/).

### Suggestions
The `#Suggestions` forum channel is a place for members of FurHaven to suggest changes and additions to FurHaven community. Some examples of items that members can submit suggestions for are the Minecraft server (Aurelium Realms) and the Discord server.

### Aurelium
`#Aurelium` is a text chat channel that the [Minecraft server's](../minecraft/index.md) chat gets fed into. It acts as a real-time feed for members by allowing them to view any chat messages that are sent publically on the Minecraft server as well as who is on the server at any given time. The channel's header topic also displays a minimalistic server status featuring uptime and current player count.

### Git
The `#Git` channel serves as an update feed for all of FurHaven's projects [hosted on Gitlab under the FurHaven organization](https://gitlab.furhaven.xyz/). Whenever a project is posted or updated, the action feed is posted via webhook to this channel; One such example is the [community support documentation](https://gitlab.com/furhaven/community-documentation) ([this knowledgebase](https://docs.furhaven.xyz/)) which is hosted on Gitlab using Gitlab Pages and MKDocs.

## Discourse
The Discourse category contains all of the main discussion channels of FurHaven's Discord server. Any discussion between members is directed to take place within these channels.

### General
The `#General` text chat channel is the central place for all community discussion to take place in. In the `#General` channel, members can engage in discussion with one another.

### Roleplay

### Art Discussion
The `#Art-Discussion` forum channel is a member's place to share and discuss particular types of art within the Furry community. Specific fetishes have dedicated general threads and members of FurHaven may also opt to create their own threads.

### Off Topic

### Bot Spam

### The Lounge

### Minechat
