# FurHaven Community Documentation
Community Documentation for the FurHaven community; Built using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), [MKDocs](https://www.mkdocs.org/), and [Material for MKDocs](https://squidfunk.github.io/mkdocs-material/).
