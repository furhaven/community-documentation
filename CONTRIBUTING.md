# Welcome to FurHaven's Community Documentation

Thank you for investing your time in contributing to our community documentation! Any contributions that you make will be reflected on [docs.furhaven.xyz](https://docs.furhaven.xyz/). 

Please familarize yourself with our [community guidelines](https://guidelines.furhaven.xyz/) to keep our community approachable and respectable.

Use the table of contents icon on the top left corner of this document to get to a specific section of this guide quickly.

## New Contributor Guide
In this guide you will get an overview of the contribution workflow from opening an issue, creating a pull request, reviewing, and merging the pull request. To get an overview of the project, read the [README](README.md).

### Contributor Requirements
Contributors to the community documentation must meet the following requirements:

- Be an active member of the FurHaven community

### Resources
Here are some resources to help you get started with open source contributions:

- [Set up Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
- [Working With Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/)

### ToDo
- Add more information about contributing documentation changes...
